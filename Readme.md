# Zulipbot

Platform to write zulip-bots in [kotlin](https://kotlinlang.org/). 
It provides a basic infrastructure to write zulip bot, see class BotRunner and ZulipBot.
At the moment there are following bots implememted:

- JiraBot - syncing zulip with jira

If you start the botrunner a file `.run` will be created in the working directory.
The bot will running until the file is deleted.

The botrunner must be installed on a machine which has access via REST-API to every machine,
which should be accessed, so zulip and jira at the moment. 
You need java 8 runtime at this machine. The distribution contains skripts to start the BotRunner.

## Installation
Be sure you have java 8 on your machine and set [JAVA_HOME](https://docs.oracle.com/cd/E19182-01/820-7851/inst_cli_jdk_javahome_t/).
1. Download the distribution
   from https://gitlab.com/api/v4/projects/12438403/packages/generic/zulipbox/<tagname like v1.3.5>
   /zulipbot.zip) or browse at https://gitlab.com/ppiag/zulipbot/-/packages.
2. Unzip or untar the downloaded file, a new folder will be created. Goto the new directory.
3. [Configure](#Configuration) the system, you find an `application.properties.sample` in the newly created folder.

## Configuration
You must rename `application.properties.sample` (which is part of the distribution) 
to `application.properties` and adjust the parameters.
The program based heavily on kotlin coroutines. 
So even if you have a lot of bots it's still fine just to allow 1 thread.

If you need specific loggings, you can use the standard log4j2 mechanism.

### Configuration-parameters in application.properties
Paramters to get access to zulip:
- zulip.url=https://####.zulipchat.com - the url of the zulip-instance.
- zulip.email=test-bot@###.zulipchat.com - the email of your bot
- zulip.apikey=secret - the apikey which will be show at the generic-bot
- zulip.name=test - the name of your bot in zulip
- zulip.fetch.retry.number=20 - the number of retries till a connection-problem will lead to a failure for zulip fetch-operations.
- zulip.fetch.retry.delay=PT20.000S - Define the delay between retries for zulip fetch-operations in form of a Java-Duration-String here 20 seconds.
- zulip.pull.retry.number=5 - the number of retries till a connection-problem will lead to a failure for zulip pull-operations.
- zulip.pull.retry.delay=PT5.000S - Define the delay between retries for zulip pull-operations in form of a Java-Duration-String here 20 seconds.

Paramters to get access to jira:
- jira.url=https://jira.company.com - the url of your jira-system.
- jira.user=jira4zulip  - the technical user of you jira-system.
- jira.password=secret - the password for jira.user.
- jira.pull.retry.number=5 - the number of retries till a connection-problem will lead to a failure for jira pull-operations.
- jira.pull.retry.delay=PT5.000S - Define the delay between retries for jira pull-operations in form of a Java-Duration-String here 20 seconds.

Common Parameters
- threadpool.size=2 - Number of threads which the bots could use.
- delay=PT5.000S - Define the delay in form of a Java-Duration-String here 5 seconds.

## Running
You can start the program with bin\zulibbot.bat or bin/zulibbot.
Please ensure that the working directory is the directory where the application.properties are.
The program will put the log-file beside in a logs-directory.
In the working-directory will be a `.run`-file created. Delete it to stop the program.

## Zulip2Jira-Bot
The command must have the form `@<name of the bot> <command> <ticketId>`.
Where command can be
* link - add a link to the stream at jira
* push - add the content of the stream to jira

On success there will be a checkmark as reaction. 
At error there will be a crossmark as reaction and a private message with details.

## Known Limitations
- If an unexpected error happens the bot will stop working. So be sure your jira/zulip is always reachable.
  The reason is, that it's a bad idea to handle a message over and over again without success.
  I will wait for feedback from practical usage and improve than the program.
- You must use a user/password for authentification, which probably a problem for cloud-users.
  Look at JiraClient to fix it.

 

