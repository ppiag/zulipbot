package de.ppi.oss.zulipbot.jira

import io.ktor.client.plugins.*
import io.ktor.client.statement.*
import kotlinx.coroutines.runBlocking
import org.junit.Ignore
import org.junit.Test

class JiraClientTest {

    private val client = JiraClient("a", "b", "c")

    @Ignore
    @Test
    fun addContentAsAttachment() {
        runBlocking {
            try {
                client.addContentAsAttachment("TPH-4514", "Dies ist ein Test", "xx.txt")
            } catch (e: ResponseException) {
                println("${e.response.status.value} -> ${e.response.readRawBytes()}")
                throw e
            }

        }
    }

    @Ignore
    @Test
    fun addExternalLink() {
        runBlocking {
            try {
                client.addExternalLink(
                    "TPH-4514",
                    "https://ctz.zulipchat.com/#narrow/stream/181903-test.2Fstream1/topic/KZulip/near/165266523",
                    """Zulip with "Quotes"."""
                )
            } catch (e: ResponseException) {
                println("${e.response.status.value} -> ${e.response.readRawBytes()}")
                throw e
            }

        }
    }
}