package de.ppi.oss.zulipbot.common

import assertk.assertThat
import assertk.assertions.isBetween
import assertk.assertions.isGreaterThan
import kotlinx.coroutines.delay
import org.junit.Test
import java.io.File

class BotRunnerTest {

    @Test
    fun testAsync() {
        val runFile = File(".testrun")
        val thread = Thread {
            Thread.sleep(2500)
            runFile.delete()
        }
        thread.start()
        val numberOfThreads = 1
        val delayInMillis = 500L

        val botACalls = mutableListOf<String>()
        val botBCalls = mutableListOf<String>()

        val runner = BotRunner(numberOfThreads, delayInMillis, runFile)
        runner.addBot(object : Bot {
            override val botName: String
                get() = "BotA"

            override suspend fun runOneCycle() {
                botACalls.add("BotA")
            }
        })
        runner.addBot(object : Bot {
            override val botName: String
                get() = "BotB"

            override suspend fun runOneCycle() {
                delay(400)
                botBCalls.add("BotB")
            }
        })
        runner.start()
        assertThat(botACalls.size).isBetween(3, 7)
        assertThat(botACalls.size).isGreaterThan(botBCalls.size)
    }
}