package de.ppi.oss.zulipbot.common


import de.ppi.oss.kzulip.client.HttpLogLevel
import de.ppi.oss.kzulip.client.ZulipClient
import de.ppi.oss.zulipbot.jira.JiraBot
import de.ppi.oss.zulipbot.jira.JiraClient
import kotlinx.coroutines.asCoroutineDispatcher
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.slf4j.LoggerFactory
import java.io.File
import java.util.concurrent.Executors
import kotlin.random.Random

private val logger = LoggerFactory.getLogger(BotRunner::class.java)

fun main() {

    val runFile = File(".run")
    val numberOfThreads = ApplicationProperties.getNumberOfWorkerThreads()
    val delayInMillis = ApplicationProperties.getDelayInMillis()

    val jiraBot = JiraBot(
        ApplicationProperties.getBotName(),
        ZulipClient(
            ApplicationProperties.getZulipUrl(),
            ApplicationProperties.getBotEmail(),
            ApplicationProperties.getBotApikey(),
            HttpLogLevel.INFO
        ),
        JiraClient(
            ApplicationProperties.getJiraUrl(),
            ApplicationProperties.getJiraUser(),
            ApplicationProperties.getJiraPassword(),
            HttpLogLevel.INFO
        )
    )

    val runner = BotRunner(numberOfThreads, delayInMillis, runFile)
    runner.addBot(jiraBot)
    runner.start()
}


class BotRunner(private val numberOfThreads: Int, private val delayInMillis: Long, private val runFile: File) {

    private val bots = mutableListOf<Pair<Long, Bot>>()

    fun addBot(bot: Bot) {
        val pair = Pair(Random.nextLong((delayInMillis * 0.75).toLong(), (delayInMillis * 1.25).toLong()), bot)
        bots.add(pair)
    }

    fun start() {
        val fixedThreadPool = Executors.newFixedThreadPool(numberOfThreads)
        val dispatcher = fixedThreadPool.asCoroutineDispatcher()
        runFile.deleteOnExit()
        if (!runFile.createNewFile()) {
            throw IllegalStateException("${runFile.absolutePath} exists. Stop starting.")
        }
        logger.info("Start running version $VERSION - $GIT_SHA. Run until ${runFile.absolutePath} will be deleted.")
        runBlocking(dispatcher) {
            bots.forEach {
                launch {
                    try {
                        runContinousWithDelay(it)
                    } catch (t: Throwable) {
                        logger.error("Error running ${it.second.botName}: ${t.message}.", t)
                        runFile.delete()
                    }
                }
            }
        }
        logger.info("Stop running. ${runFile.absolutePath} is deleted.")
        fixedThreadPool.shutdown()
    }

    private suspend fun runContinousWithDelay(botDefinition: Pair<Long, Bot>) {
        while (runFile.exists()) {
            delay(botDefinition.first)
            botDefinition.second.runOneCycle()
        }
    }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            main()
        }
    }
}