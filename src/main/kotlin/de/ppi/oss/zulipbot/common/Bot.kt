package de.ppi.oss.zulipbot.common

interface Bot {

    suspend fun runOneCycle()

    val botName:String
}