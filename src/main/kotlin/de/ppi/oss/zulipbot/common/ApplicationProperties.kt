package de.ppi.oss.zulipbot.common

import org.slf4j.LoggerFactory.getLogger
import java.io.File
import java.io.FileReader
import java.time.Duration
import java.util.*
import kotlin.system.exitProcess

// TODO niels 08.05.2019: Auf lange Sicht braucht man hier was dynamischeres, z.B.
// Allgemeine configs die in Json gepackt werden. Die Anzahl der Workerthreads solte auch nur einmal definiert werden.
/**
 * Comfort-object to access the application.properties.
 */
object ApplicationProperties {

    private val logger = getLogger(ApplicationProperties::class.java)

    private val properties = readProperties()

    fun getJiraUrl(): String {
        return properties.getProperty("jira.url")
    }

    fun getJiraUser(): String {
        return properties.getProperty("jira.user")
    }

    fun getJiraPassword(): String {
        return properties.getProperty("jira.password")
    }


    fun getZulipUrl(): String {
        return properties.getProperty("zulip.url")
    }

    fun getBotName(): String {
        return properties.getProperty("zulip.name")
    }

    fun getBotEmail(): String {
        return properties.getProperty("zulip.email")
    }

    fun getBotApikey(): String {
        return properties.getProperty("zulip.apikey")
    }

    fun getNumberOfWorkerThreads(): Int {
        return Integer.valueOf(properties.getProperty("threadpool.size"))
    }

    fun getDelayInMillis(): Long {
        return Duration.parse(properties.getProperty("delay")).toMillis()
    }

    fun getZulipFetchRetryDelayInMillis(): Long {
        return Duration.parse(properties.getProperty("zulip.fetch.retry.delay")).toMillis()
    }

    fun getZulipFetchRetryNumbers(): Int {
        return Integer.valueOf(properties.getProperty("zulip.fetch.retry.number", "20"))
    }


    fun getZulipPushRetryDelayInMillis(): Long {
        return Duration.parse(properties.getProperty("zulip.push.retry.delay", "PT5.000S")).toMillis()
    }

    fun getZulipPushRetryNumbers(): Int {
        return Integer.valueOf(properties.getProperty("zulip.push.retry.number", "6"))
    }

    fun getJiraPushRetryDelayInMillis(): Long {
        return Duration.parse(properties.getProperty("jira.push.retry.delay", "PT5.000S")).toMillis()
    }

    fun getJiraPushRetryNumbers(): Int {
        return Integer.valueOf(properties.getProperty("jira.push.retry.number", "6"))
    }

    private fun readProperties(): Properties {
        val properties = Properties()
        val propertiesFileName = "application.properties"
        var propertiesFile = File(propertiesFileName)
        if (!propertiesFile.exists()) {
            val loadClass = try {
                // First try if is a Sprint-Boot-Runner-jar.
                ClassLoader.getSystemClassLoader().loadClass("org.springframework.boot.loader.Launcher")
            } catch (e: ClassNotFoundException) {
                ApplicationProperties::class.java
            }
            val location = loadClass.protectionDomain.codeSource.location
            val currentLocation = File(location.toURI())
            propertiesFile = if (currentLocation.isDirectory) {
                File(currentLocation, propertiesFileName)
            } else {
                File(currentLocation.parent, propertiesFileName)
            }
        }

        if (propertiesFile.exists()) {
            logger.info("Load properties from : ${propertiesFile.absolutePath}")
        } else {
            logger.error("Can't load properties from : ${propertiesFile.absolutePath}")
            exitProcess(1)
        }
        properties.load(FileReader(propertiesFile))
        return properties
    }


}