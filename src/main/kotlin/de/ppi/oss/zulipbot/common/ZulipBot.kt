package de.ppi.oss.zulipbot.common

import de.ppi.oss.kzulip.api.common.Emoji
import de.ppi.oss.kzulip.api.common.Flag
import de.ppi.oss.kzulip.api.messages.*
import de.ppi.oss.kzulip.client.BadZulipResponseStatusException
import de.ppi.oss.kzulip.client.ZulipClient
import io.ktor.client.call.ReceivePipelineException
import io.ktor.http.HttpStatusCode
import kotlinx.coroutines.delay
import org.slf4j.LoggerFactory
import java.net.ConnectException
import java.net.SocketTimeoutException

private val logger = LoggerFactory.getLogger(ZulipBot::class.java)

/**
 * Baseclass to create a [Bot] for zulip.
 */
abstract class ZulipBot(override val botName: String, protected val zulipClient: ZulipClient) : Bot {

    protected suspend fun publishSucces(md: GetMessageDetail) {
        runWithRetries(
            "Zulip publish success",
            ApplicationProperties.getZulipPushRetryNumbers(),
            ApplicationProperties.getZulipPushRetryDelayInMillis()
        ) {
            zulipClient.addReaction(ReactionRequest(md.id, Emoji.CHECK_MARK))
        }
    }

    protected suspend fun publishError(md: GetMessageDetail, errorMessage: String) {
        runWithRetries(
            "Zulip publish error emoticon",
            ApplicationProperties.getZulipPushRetryNumbers(),
            ApplicationProperties.getZulipPushRetryDelayInMillis()
        ) {
            zulipClient.addReaction(ReactionRequest(md.id, Emoji.CROSS_MARK))
        }
        runWithRetries(
            "Zulip publish error message",
            ApplicationProperties.getZulipPushRetryNumbers(),
            ApplicationProperties.getZulipPushRetryDelayInMillis()
        ) {
            zulipClient.sendMessage(SendMessageRequest(md.senderEmail, md.subject, errorMessage, true))
        }
    }

    protected suspend fun markAsRead(md: GetMessageDetail) {
        runWithRetries(
            "Zulip mark as read",
            ApplicationProperties.getZulipPushRetryNumbers(),
            ApplicationProperties.getZulipPushRetryDelayInMillis()
        ) {
            zulipClient.updateMessageFlag(
                UpdateMessageFlagRequest(
                    messages = listOf(md.id),
                    flag = Flag.READ,
                    op = UpdateMessageFlagOperation.ADD
                )
            )
        }
    }

    /**
     * Retries the given work in case of ConnectException, hopefully a temporary problem.
     * @param callInfo some information about the context.
     * @param retries number of retries (Default: 12)
     * @param delay delay between retries in milliseconds (Default:5000)
     * @param work function which do the specific work.
     */
    protected suspend fun <T> runWithRetries(
        callInfo: String,
        retries: Int = 12,
        delay: Long = 5000,
        work: suspend () -> T
    ): T {
        var remainingRetries = retries
        while (remainingRetries > 1) {
            remainingRetries--
            try {
                return work()
            } catch (e: ConnectException) {
                logger.warn("${e.javaClass.simpleName} at $botName with $callInfo: ${e.message}. Remaining retries: $remainingRetries")
            } catch (s: SocketTimeoutException) {
                logger.warn("${s.javaClass.simpleName} at $botName with $callInfo: ${s.message}. Remaining retries: $remainingRetries")
            } catch (r: ReceivePipelineException) {
                val cause = r.cause
                if (cause is BadZulipResponseStatusException) {
                    if (cause.statusCode == HttpStatusCode.BadGateway) {
                        // Happens at reboot
                        logger.warn("Badgateway-Exception at $botName with $callInfo: ${cause.message?.take(90)}. Remaining retries: $remainingRetries")
                    } else {
                        logger.error("BadZulipResponseStatusException with ${cause.statusCode} ${cause.statusCode == HttpStatusCode.BadGateway}")
                        throw cause
                    }
                } else {
                    logger.warn("No retry for ReceivePipelineException with cause ${r.cause.javaClass.name}")
                    throw r
                }
            } catch (t: Throwable) {
                logger.warn("No retry for ${t.javaClass.name}")
                throw t
            }
            delay(delay)
        }
        return work()
    }
}