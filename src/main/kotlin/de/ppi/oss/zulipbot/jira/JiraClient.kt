package de.ppi.oss.zulipbot.jira

import com.atlassian.jira.rest.client.api.domain.Issue
import com.atlassian.jira.rest.client.internal.async.AsynchronousJiraRestClientFactory
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import de.ppi.oss.kzulip.client.HttpLogLevel
import io.ktor.client.*
import io.ktor.client.engine.apache.*
import io.ktor.client.plugins.*
import io.ktor.client.plugins.auth.*
import io.ktor.client.plugins.auth.providers.*
import io.ktor.client.plugins.logging.*
import io.ktor.client.request.*
import io.ktor.content.*
import io.ktor.http.*
import kotlinx.coroutines.CancellableContinuation
import kotlinx.coroutines.delay
import kotlinx.coroutines.suspendCancellableCoroutine
import org.slf4j.LoggerFactory
import java.io.ByteArrayInputStream
import java.net.URI
import java.nio.charset.Charset
import kotlin.coroutines.resume

/**
 * Client to make the jira-call which are needed.
 */
class JiraClient(
    private val siteUrl: String,
    jiraUser: String,
    jiraPwd: String,
    logLevel: HttpLogLevel = HttpLogLevel.ALL
) {
    private val logger = LoggerFactory.getLogger(JiraClient::class.java)
    private val client = HttpClient(Apache) {
        expectSuccess = true
        install(Auth) {
            basic {
                credentials {
                    BasicAuthCredentials(username = jiraUser, password = jiraPwd)
                }
                sendWithoutRequest { true }
            }
        }
        install(Logging) {
            level = LogLevel.valueOf(logLevel.name)
        }
        install(UserAgent) {
            agent = "ZulipBot/API"
        }
    }

    private val mapper = jacksonObjectMapper()

    private val jiraClient =
        AsynchronousJiraRestClientFactory().createWithBasicHttpAuthentication(URI(siteUrl), jiraUser, jiraPwd)
            .issueClient

    @Throws(JiraException::class, NotFoundException::class)
    suspend fun addContentAsAttachment(ticketId: String, content: String, filename: String) {
        try {
            client.get("$siteUrl/rest/api/2/issue/$ticketId")
        } catch (e: ResponseException) {
            if (e.response.status == HttpStatusCode.NotFound) {
                logger.error("Can't find ticket $ticketId.", e)
                throw NotFoundException(ticketId)
            }
            logger.error("Can't update ticket $ticketId.", e)
            // TODO niels 21.10.2020: Besser wäre es den Body zu bekommen.
            throw JiraException(e.message ?: "")
        }
        try {
            val issue = loadIssue(ticketId)
            val promise = jiraClient.addAttachment(
                issue.attachmentsUri,
                ByteArrayInputStream(content.toByteArray(Charset.forName("UTF-8"))),
                filename
            )
            while (!promise.isDone) {
                delay(100L)
            }
        } catch (e: java.lang.Exception) {
            val message = "Problem attach information: $e.message"
            logger.error(message, e)
            throw JiraException(message)
        }

    }

    private suspend fun loadIssue(ticketId: String): Issue {
        return suspendCancellableCoroutine { continuation: CancellableContinuation<Issue> ->
            jiraClient.getIssue(ticketId).done { issue ->
                continuation.resume(issue)
            }
        }
    }


    @Throws(JiraException::class, NotFoundException::class)
    suspend fun addExternalLink(ticketId: String, link: String, title: String) {
        // Not supported by java-api
        try {
            client.post {
                url(URI.create("$siteUrl/rest/api/2/issue/$ticketId/remotelink").toURL())
                val objectString = mapper.writeValueAsString(LinkDataObject(link, title))
                setBody(TextContent("""{"object": $objectString}""", ContentType.Application.Json))
                header("X-Atlassian-Token", "no-check")
            }
        } catch (e: ResponseException) {
            if (e.response.status == HttpStatusCode.NotFound) {
                logger.error("Can't find ticket $ticketId.", e)
                throw NotFoundException(ticketId)
            }
            logger.error("Can't update ticket $ticketId.", e)
            throw JiraException(e.message ?: "")
        }
    }

    class NotFoundException(ticketId: String) : JiraException("Jira-issue $ticketId not found.")

    open class JiraException(message: String) : Exception(message)

    data class LinkDataObject(val url: String, val title: String)
}