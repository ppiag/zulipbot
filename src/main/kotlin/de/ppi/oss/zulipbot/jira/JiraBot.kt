package de.ppi.oss.zulipbot.jira

import de.ppi.oss.kzulip.api.messages.GetMessageDetail
import de.ppi.oss.kzulip.api.messages.GetMessageRequest
import de.ppi.oss.kzulip.api.messages.IsValue
import de.ppi.oss.kzulip.api.messages.NarrowBuilder
import de.ppi.oss.kzulip.client.ZulipClient
import de.ppi.oss.zulipbot.common.ApplicationProperties
import de.ppi.oss.zulipbot.common.ZulipBot
import org.slf4j.LoggerFactory
import java.time.Instant
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.util.*


private val logger = LoggerFactory.getLogger(JiraBot::class.java)

/**
 * Bot to link jira with zulip.
 */
class JiraBot(botName: String, zulipClient: ZulipClient, private val jiraClient: JiraClient) :
    ZulipBot(botName, zulipClient) {

    private val createHtml = false

    private val usage = """The command must have the form @$botName <command> <ticketId>.
            |Where command can be
            |* link - add a link to the stream at jira
            |* push - add the content of the stream to jira
            |You must create a _new_ comment to fix it. Edit isn't enough.
        """.trimMargin()

    private val htmlHeader = """
        <!doctype html>
        <head>
        </head>
        <body>
    """.trimIndent()

    private val commandMatcher =
        Regex("""\*\*$botName\*\* *\b(\w*)\b *([A-Za-z0-9]{2,10}-[0-9]{2,6})""", RegexOption.IGNORE_CASE)

    override suspend fun runOneCycle() {
        runWithRetries(
            "Zulip get messages",
            ApplicationProperties.getZulipFetchRetryNumbers(),
            ApplicationProperties.getZulipFetchRetryDelayInMillis()
        ) {
            zulipClient.getMessage(
                GetMessageRequest(
                    useFirstUnreadAnchor = true,
                    narrow = NarrowBuilder().ist(IsValue.MENTIONED).build()
                )
            )
        }.messages.forEach { md: GetMessageDetail ->
            handleContent(md)
            markAsRead(md)
        }
    }


    private suspend fun handleContent(md: GetMessageDetail) {
        logger.debug("Handle: ${md.content} with topic ${md.subject} and id ${md.id}")
        val matchResult = commandMatcher.find(md.content)?.groupValues
        if (matchResult == null || matchResult.size != 3) {
            publishError(md, usage)
            return
        }
        val command = matchResult[1]
        val ticketId = matchResult[2]

        when (command.lowercase(Locale.getDefault())) {
            "link" -> createLink(md, ticketId)
            "push" -> pushContent(md, ticketId)
            else -> publishError(md, "${usage}Unkown command $command.")
        }
    }

    private suspend fun pushContent(md: GetMessageDetail, ticketId: String) {
        val streamId = md.streamId
        if (streamId == null) {
            publishError(md, "Can't find stream, because stream-id is missing.")
            return
        }
        val streamName = zulipClient.getStreamInfo(streamId)?.name
        if (streamName == null) {
            publishError(md, "Can't find stream with id streamId")
            return
        }
        val collectMessages = collectMessages(md, streamName)
        val messageSummary = createSummary(collectMessages)
        try {
            runWithRetries(
                "Jira add attachment",
                ApplicationProperties.getJiraPushRetryNumbers(),
                ApplicationProperties.getJiraPushRetryDelayInMillis()
            ) {
                if (createHtml) {
                    jiraClient.addContentAsAttachment(
                        ticketId,
                        "$htmlHeader${zulipClient.renderMessage(messageSummary).rendered}</body></html>",
                        "zulipdiscussion${md.subject}.html"
                    )
                } else {
                    jiraClient.addContentAsAttachment(ticketId, messageSummary, "zulipdiscussion${md.subject}.md")
                }
            }
            publishSucces(md)
        } catch (e: JiraClient.JiraException) {
            publishError(md, "Error attach file: ${e.message}.")
        }
    }


    private suspend fun collectMessages(md: GetMessageDetail, streamName: String): List<GetMessageDetail> {
        var narrow = NarrowBuilder().stream(streamName)
        val subject = md.subject
        if (subject != null) {
            narrow = narrow.topic(subject)
        }

        var anchor = 1L
        val messages = mutableListOf<GetMessageDetail>()
        while (anchor > 0) {
            val messageResponse = zulipClient.getMessage(
                GetMessageRequest(
                    anchor = 1, numAfter = 1000,
                    narrow = narrow.build()
                )
            )
            messages.addAll(messageResponse.messages)
            anchor = if (messageResponse.foundNewest) {
                0
            } else {
                messageResponse.messages.last().id
            }
        }
        return messages
    }

    private fun createSummary(collectedMessages: List<GetMessageDetail>): String {
        var lastUser: String? = null
        val summary = StringBuilder()
        for (message in collectedMessages) {
            val senderFullName = message.senderFullName
            if (lastUser == null) {
                summary.append(getSendInformation(message))
            } else if (lastUser != senderFullName) {
                summary.append("---\n${getSendInformation(message)}")
            }
            lastUser = senderFullName
            summary.append("${message.content}\n")
        }
        return summary.toString()
    }

    private fun getSendInformation(message: GetMessageDetail): String {
        val time = Instant.ofEpochSecond(message.timestamp).atZone(ZoneId.systemDefault())
        val timeAsString = time.format(DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm"))
        return "${message.senderFullName} ($timeAsString): "
    }

    private suspend fun createLink(md: GetMessageDetail, ticketId: String) {
        val link = "${zulipClient.site}/#narrow/near/${md.id}"
        try {
            runWithRetries(
                "Jira add link",
                ApplicationProperties.getJiraPushRetryNumbers(),
                ApplicationProperties.getJiraPushRetryDelayInMillis()
            ) {
                val streamId = md.streamId
                val title = if (streamId != null) {
                    val streamInfo = zulipClient.getStreamInfo(streamId)
                    "Zulip: #${streamInfo?.name} > ${md.subject}"
                } else {
                    "Zulip"
                }
                jiraClient.addExternalLink(ticketId, link, title)
                publishSucces(md)
            }
        } catch (e: JiraClient.JiraException) {
            publishError(md, "Error creating link: ${e.message}.")
        }
    }
}