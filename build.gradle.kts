import org.jetbrains.kotlin.gradle.dsl.JvmTarget
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

val log4j2_version: String by project
val slf4j_version: String by project
val ktor_version: String by project
val kotlin_version: String by project
val kzulip_version: String by project
val coroutines_version: String by project
val VERSION_NAME: String by project

plugins {
    application
    kotlin("jvm") version "2.1.10"
    id("com.peterabeles.gversion") version "1.5.0"
}

group = "de.ppi.oss"
version = VERSION_NAME



configurations.all {
    resolutionStrategy.cacheDynamicVersionsFor(10, "minutes")
}



application {
    mainClass.set("de.ppi.oss.zulipbot.common.BotRunner")
}

repositories {
    mavenLocal()
    mavenCentral()
    maven{
        url = uri("https://packages.atlassian.com/mvn/maven-atlassian-external/")
    }
    maven {
        url = uri("https://oss.sonatype.org/content/repositories/snapshots/")
        mavenContent {
            snapshotsOnly()
        }
    }
}

dependencies {
    implementation("de.ppi.oss:kzulip:$kzulip_version")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8:$kotlin_version")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:$coroutines_version")

    // Jira-Client
    implementation("com.atlassian.jira:jira-rest-java-client-core:6.0.2")
    implementation("io.atlassian.fugue:fugue:3.0.0")

    // Ktor
    implementation("io.ktor:ktor-server-netty:$ktor_version")
    implementation("org.apache.logging.log4j:log4j-api:$log4j2_version")
    implementation("org.apache.logging.log4j:log4j-core:$log4j2_version")
    implementation("org.apache.logging.log4j:log4j-slf4j-impl:$log4j2_version")

    implementation("io.ktor:ktor-client-core:$ktor_version")
    implementation("io.ktor:ktor-client-core-jvm:$ktor_version")
    implementation("io.ktor:ktor-client-json:$ktor_version")
    implementation("io.ktor:ktor-client-jackson:$ktor_version")
    implementation("io.ktor:ktor-client-apache:$ktor_version")
    implementation("io.ktor:ktor-client-auth:$ktor_version")
    implementation("io.ktor:ktor-client-logging-jvm:$ktor_version")
    implementation("io.ktor:ktor-client-logging:$ktor_version")
    // Test
    testImplementation("junit:junit:4.13.2")
    testImplementation("com.willowtreeapps.assertk:assertk-jvm:0.13")
    implementation("org.slf4j:slf4j-api:$slf4j_version")
}

kotlin {
    jvmToolchain(21)
}

val compileKotlin: KotlinCompile by tasks
compileKotlin.compilerOptions {
    jvmTarget.set(JvmTarget.JVM_21)
}
val compileTestKotlin: KotlinCompile by tasks
compileTestKotlin.compilerOptions {
    jvmTarget.set(JvmTarget.JVM_21)
}


val genSrcDir = "build/src/gen/kotlin"
sourceSets {
    main {
        java {
            srcDir(genSrcDir)
        }
    }
}

compileKotlin.dependsOn("createVersionFile")

distributions {
    main {
        version=""
        contents {
            from("Readme.md")
            from("application.properties.sample")
        }
    }
}

gversion {
    srcDir = genSrcDir           // path is relative to the sub-project by default
    // Gradle variables can also be used
    // E.g. "${project.rootDir}/module/src/main/java"
    classPackage = "de.ppi.oss.zulipbot.common"
    className = "GBuildInfo"                // optional. If not specified GVersion is used
    dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'" // optional. This is the default
    timeZone = "UTC"                      // optional. UTC is default
    debug = false                      // optional. print out extra debug information
    language = "kotlin"                     // optional. Can be Java or Kotlin, case insensitive
    explicitType = false                      // optional. Force types to be explicitly printed
}
